let course = [];

/* add course */

let postCourse = (id, name, description, price, isActive) => {
    let courseToAdd = { id, name, description, price, isActive }

    course.push(courseToAdd);

    console.log(`Successfully added course ${name}. the price is ${price}`);

}

postCourse(1, 'JSON', 'Javascript', 1290, true);
postCourse(2, 'Python', 'Python Programming', 134, true);


let searchCourse = (array, id) => {
    let foundCourse = course.find(element => id);
    return foundCourse;

};
searchCourse(course, 'JSON');
console.log(course);

let deleteCourse = (id) => {
    for (let i = 0; i <= course.length - 1; i++) {
        let tempVariable = course[i];
        if (tempVariable.id == id) {
            course.pop(i);
            console.log(`Succesfully deleted course ${tempVariable.name}.`);
        } else {
            console.log(`Unable to delete course ${tempVariable.name}`);
        }
    }
}

deleteCourse(1);
console.log(course);